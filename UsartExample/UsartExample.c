﻿/* Simple USART receive and transmit example */
/* Note: using Proteus remember to set microcontroller frequency to 8mhz */

#include <avr/io.h>
#define F_CPU 8000000UL

void UsartInit()
{
    UCSRB |= (1<<RXEN)|(1<<TXEN); // enable Receive and Transmit
    UCSRC |= (1<<URSEL)|(1<<UCSZ1)|(1<<UCSZ0); // 8 data bits, 1 stop bit, parity None
	
    unsigned int BAUD = 9600;
    BAUD = (F_CPU/16/BAUD)-1;
    UBRRH = BAUD>>8; // set 16-bit baud rate register
    UBRRL = BAUD;
}

void UsartSendByte(uint8_t data)
{
    while(!(UCSRA & (1<<UDRE))){}; // waiting for previous byte transfer
    UDR = data; // send data
}

void UsartSendString(char msg[])
{
    unsigned int i;
    for(i=0; i<strlen(msg); i++)
        UsartSendByte(msg[i]);
}

uint8_t UsartGetByte()
{
    while(!(UCSRA & (1<< RXC))){}; // waiting for input byte 
    return UDR; // read data
}


int main(void)
{
    DDRB |= (1<<0);
    UsartInit();
	
    while(1)
    {
        uint8_t ch = UsartGetByte();
        UsartSendString("You sent: ");
        UsartSendByte(ch);
		
        if(ch == '1')
        {
            PORTB |= (1<<0);
            UsartSendString(" Led is ON!");
        }			
        else if(ch == '0')
        {
            PORTB &= ~(1<<0);
            UsartSendString(" Led is OFF!");
        }			
			
        UsartSendByte('\r');
   }
}