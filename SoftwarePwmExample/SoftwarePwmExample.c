/* Software PWM implementation example */
/* Uses 8-bit timer0 */

/* timerVal increments by 1 on every timer0 overflow, PWM pins go low on compare match and high on bottom */
/* This results in software PWM with frequency F_CPU/256 == 31250 Hz */

#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 8000000UL;

unsigned char timerVal = 0;
unsigned char PB4val = 0;
unsigned char PB5val = 0;


ISR(TIMER0_OVF_vect)
{
    if(timerVal < 256) 
        timerVal++;
    else
    {
        timerVal = 0;
        PORTB |= (1<<4);
        PORTB |= (1<<5);
    }
        
    if(timerVal == PB4val) 
        PORTB &= ~(1<<4);
    
    if(timerVal == PB5val)
        PORTB &= ~(1<<5);
}

int main(void)
{
    DDRB |= (1<<4)|(1<<5);
        
    TCCR0 |= (1<<CS00); // start timer, no prescaling
    TIMSK |= (1<<TOIE0); // enable timer0 overflow interrupt
    sei();
    
    PB4val = 128; //50%
    PB5val = 16; //6%
    
    
    while(1)
    {

    }
}