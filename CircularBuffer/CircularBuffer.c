/* Simple circular buffer implementation */
/* Note: Buffer size could be specified using malloc or some macro for each buffer separately */

#include <avr/io.h>
#define F_CPU 8000000UL;

#define BUFFER_SIZE 32

typedef struct
{
	uint8_t dataStorage[BUFFER_SIZE];
	uint32_t start;
	uint32_t next;
	uint32_t numberOfActive;

}CircularBuffer;

void bufferPush(CircularBuffer *buffer, uint8_t value)
{
	buffer->dataStorage[buffer->next] = value;
	buffer->next = (buffer->next + 1) % BUFFER_SIZE;

	if(buffer->numberOfActive < BUFFER_SIZE)
	{
		buffer->numberOfActive++;
	}
	else
	{
		buffer->start = (buffer->start + 1) % BUFFER_SIZE;
	}
}

uint8_t bufferPop(CircularBuffer *buffer)
{
	uint8_t value;

	if(buffer->numberOfActive == 0)
	{
		return 0;
	}

	value = buffer->dataStorage[buffer->start];
	buffer->start = (buffer->start + 1) % BUFFER_SIZE;

	buffer->numberOfActive--;
	return value;
}

int main(void)
{
	CircularBuffer buffer1 = {{0}};
	bufferPush(&buffer1, 10);
	bufferPush(&buffer1, 20);
	bufferPush(&buffer1, 30);
	bufferPush(&buffer1, 40);
	bufferPush(&buffer1, 50);
	bufferPush(&buffer1, 60);
	
	bufferPop(&buffer1);
	bufferPop(&buffer1);
	bufferPop(&buffer1);
	
	while (1)
	{
	}
}