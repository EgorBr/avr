- This example shows how to use hardware I2C (TWI) interface as Master.
- Useful info about I2C: 
   http://easyelectronics.ru/interface-bus-iic-i2c.html
   https://ru.wikipedia.org/wiki/I%C2%B2C
   http://www.embedds.com/programming-avr-i2c-interface/
- Tested with Atmega8.
- See wiring in wiring.png