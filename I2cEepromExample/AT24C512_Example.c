﻿/* Simple I2C EEPROM (AT24C512) reading and writing example. */
/* Note: using Proteus remember to use pullups unstead of usual resistors */

#include <avr/io.h>
#include <avr/delay.h>
#include <compat/twi.h>
#include "twi.h"

#define F_CPU 8000000UL

// WP (Write Protection) -> GND
int main(void)
{
	TwiInit();
	
	// Writing
	TwiStart();
	TwiWrite(0b10100000); // A0,A1,A2 -> GND
	TwiWrite(0x00); // first part of writing address
	TwiWrite(0x00); // second part of writing address
	TwiWrite(0x3f); // write data byte (up to 127 data words, page writing - addr autoincrement)
	TwiWrite(0xf0); // write data byte
	TwiWrite(0x02); // write data byte
	TwiStop();
	_delay_ms(5); // wait write cycle time (see at24c512 datasheet)
	
	
	// Dummy Write before actual reading (see at24c512 datasheet)
	TwiStart();
	TwiWrite(0b10100000);
	TwiWrite(0x00); // first part of reading address
	TwiWrite(0x00); // second part of reading address
	TwiStop();	
	_delay_ms(5);
	
	// Reading
	TwiStart();
	TwiWrite(0b10100001);
	uint8_t data = TwiReadNACK(); // read one byte
	TwiStop();
	
	
	DDRB = 0xff;
	PORTB = data;
	
    while(1)
    {
		
    }
}