// Simple example of 3x 7-Segment indicator multiplexing
// Common anode
// 7-Segment pinout may vary

#define  F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

// PC0, PC1, PC2 -- on/off indicators
// PD0 - PD7 -- current indicator value


volatile uint16_t sevenSegVal = 0; // [0 - 999]
volatile uint8_t currentSegment = 0;

const uint8_t segmentValues[] = 
{
    0b00000011, // 0
    0b10011111, // 1
    0b00100101, // 2
    0b00001101, // 3
    0b10011001, // 4
    0b01001001, // 5
    0b01000001, // 6
    0b00011111, // 7
    0b00000001, // 8
    0b00001001, // 9
    0b11111111  // 
};

// position count from end
uint8_t getDigit(uint16_t num, uint16_t pos)
{
    uint16_t dd = 1;
    for(uint16_t i=0; i<pos; i++)
    {
        dd *= 10;
    }   
    
    return (num / dd) % 10;
}

// Period = 16.384 ms
ISR(TIMER0_OVF_vect)
{
    if(currentSegment == 0)
    {
        PORTC &= ~((1<<1)|(1<<2)); // 1 and 2 indicators off
        PORTC |= (1<<0);           // 0 indicator on
        
        if(sevenSegVal > 99)
        {
            uint16_t digit = getDigit(sevenSegVal, 2);
            PORTD = segmentValues[digit];
        }
        else
        {
            PORTD = segmentValues[10];
        }

    }
    else if(currentSegment == 1)
    {
        PORTC &= ~((1<<0)|(1<<2)); // 0 and 2 indicators off
        PORTC |= (1<<1);           // 1 indicator on
        
        if(sevenSegVal > 9)
        {
            uint16_t digit = getDigit(sevenSegVal, 1);
            PORTD = segmentValues[digit];
        }
        else
        {
            PORTD = segmentValues[10];
        }
    }
    else if(currentSegment == 2)
    {
        PORTC &= ~((1<<0)|(1<<1)); // 0 and 1 indicators off
        PORTC |= (1<<2);           // 2 indicator on
        
        uint16_t digit = getDigit(sevenSegVal, 0);
        PORTD = segmentValues[digit];
    }
    
    currentSegment++;
    if(currentSegment == 3)
    {
        currentSegment = 0;
    }
        
    TCNT0 = 128; // reinit counter
}

int main(void)
{
    DDRD = 0xff;
    DDRC |= (1<<0)|(1<<1)|(1<<2);
    
    // Period = 16.384 ms
    TCCR0 = (1<<CS02) | (1<<CS00);
    TIMSK |= (1<<TOIE0);
    TCNT0 = 128;
    sei();
    
    while (1)
    {
        if(sevenSegVal <= 999)
        {
            sevenSegVal++;
        }
        else
        {
            sevenSegVal = 0;
        }

        _delay_ms(100);
    }
}
