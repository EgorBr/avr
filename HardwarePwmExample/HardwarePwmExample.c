/* Atmega8 has 3 hardware PWM channels */
/* 2 PWM on Timer1 and 1 on Timer2 */

/* Note:  Due to the high frequency Fast PWM is best used for DAC, fading LEDs, rectification and power regulation. */
/*        Phase Corrected PWM is recommended for motor control. It doesn't have phase shift.*/


#include <avr/io.h>
#define F_CPU 8000000UL;

int main(void)
{
    
    DDRB |= (1<<1)|(1<<2)|(1<<3); // OC1A, OC1B, OC2
    
    // Timer1
    TCCR1A |= (1<<COM1A1)|(1<<COM1B1);  // OC1A and OC1B high at bottom, low on Compare Match
    TCCR1A |= (1<<WGM10);   // phase correct, 8 bit
    TCCR1B |= (1<<WGM10);   // phase correct, 8 bit
    TCCR1B |= (1<<CS10);    // start timer, no prescaling
    
    OCR1A = 128; // 50%
    OCR1B = 32; // 13%
    
    
    // Timer2
    TCCR2 |= (1<<COM21); // OC2 high at bottom, low on Compare Match
    TCCR2 |= (1 << WGM21) | (1 << WGM20); // Fast PWM
    TCCR2 |= (1 << CS20); // start timer, no prescaling
    
    OCR2 = 64; // 25%
    
    
    while(1)
    {
        
    }
}