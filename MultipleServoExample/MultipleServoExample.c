/* Multiple servo control */
/* This example shows how to control up to 10 servos using only one 16-bit timer */
/* Usually servo controls by signal with period of 20 ms (freq = 50 Hz) and pulse width from 1ms to 2 ms. See control.png */
/* The main idea is to send control signals to servos one by one, by changing value in OCR1A */
/* See pulses.png for better understanding */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/delay.h>
#define F_CPU 8000000UL;

#define CYCLE 2000 // 1/10 of period

// angle measures in microseconds 1000 - 2000
// Note: volatile is extremely important, because this values can be accessed from interrupt (not normal flow of execution)
volatile unsigned short angle1 = 1000; 
volatile unsigned short angle2 = 1000;  
volatile unsigned short angle3 = 1000;
volatile unsigned short tick = 0;


ISR(TIMER1_COMPA_vect)
{
    if (tick == 0)
    {
        PORTB |= (1<<0);
        OCR1A = angle1;
    }
    else if (tick == 1)
    {
        PORTB &= ~(1<<0);
        OCR1A = CYCLE - angle1;
    }
    else if (tick == 2)
    {
        PORTB |= (1<<1);
        OCR1A = angle2;
    }
    else if (tick == 3)
    {
        PORTB &= ~(1<<1);
        OCR1A = CYCLE - angle2;
    }
    else if (tick == 4)
    {
        PORTB |= (1<<2);
        OCR1A = angle3;
    }
    else if (tick == 5)
    {
        PORTB &= ~(1<<2);
        OCR1A = CYCLE - angle3;
    }
    
    tick++;
    if(tick >= 6)
        tick = 0;
}

void main ()
{
    DDRB |= (1<<0)|(1<<1)|(1<<2);
    
    TCCR1B |= (1 << WGM12); // CTC mode (Clear Timer on Compare)
    TCCR1B |= (1<<CS11); // start timer, prescaling 8
    TIMSK |= (1<<OCIE1A); // enable timer1 compare interrupt
    sei();
    
    angle1 = 1333; // 60 deg
    angle2 = 1500; // 90 deg
    angle3 = 1233; // 42 deg
    
    while (1)
    {
        // feel free to change angles here
    }
}