// Software UART example
// no timers, just simple delays
// baudrate 9600, 1 stop bit, 8 data bits, no parity check
// Blocking mode

#define F_CPU 8000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define SOFT_UART_PORT       PORTC
#define SOFT_UART_PIN        PINC
#define SOFT_UART_DDR        DDRC
#define SOFT_UART_RX_PIN     0
#define SOFT_UART_TX_PIN     1
#define SOFT_UART_BAUD       9600

#define SOFT_UART_DELAY      1000.0 / SOFT_UART_BAUD


void soft_uart_init()
{
    SOFT_UART_DDR |= (1<<SOFT_UART_TX_PIN);
    SOFT_UART_PORT |= (1<<SOFT_UART_TX_PIN);    
}


// blocking function
void soft_uart_write_byte(uint8_t data)
{
    SOFT_UART_PORT &= ~(1<<SOFT_UART_TX_PIN); // START bit
    _delay_ms(SOFT_UART_DELAY);
    
    for(uint8_t i=0; i<8; i++)
    {
        if(data & 0x01)
        {
            SOFT_UART_PORT |= (1<<SOFT_UART_TX_PIN);
        }
        else
        {
            SOFT_UART_PORT &= ~(1<<SOFT_UART_TX_PIN);
        }
        
        data >>= 1;
        _delay_ms(SOFT_UART_DELAY);
    }
    
    SOFT_UART_PORT |= (1<<SOFT_UART_TX_PIN); // stop bit
    _delay_ms(SOFT_UART_DELAY);
}


// (there is no STOP check)
uint8_t soft_uart_read_byte()
{
    uint8_t data = 0x00;
    
    while(SOFT_UART_PIN & (1<<SOFT_UART_RX_PIN)){}; // wait for START bit
    _delay_ms(SOFT_UART_DELAY * 1.5);
    
    for(uint8_t i=0; i<8; i++)
    {
        if(SOFT_UART_PIN & (1<<SOFT_UART_RX_PIN))
        {
            data |= (1<<i);
        }
        _delay_ms(SOFT_UART_DELAY);
    }
    
    _delay_ms(SOFT_UART_DELAY * 0.5); // wait until end of transmission

    return data;
}


int main(void)
{
    soft_uart_init();

    uint8_t incoming_byte;
    
    while(1) 
    {
        cli();
        incoming_byte = soft_uart_read_byte();
        sei();
        
        cli();
        soft_uart_write_byte(incoming_byte);
        sei();
                
    }
}

