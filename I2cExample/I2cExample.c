﻿/* I2C (TWI) usage example */
/* Note: using Proteus remember to use pullups unstead of usual resistors */
/* Useful info about I2C: http://easyelectronics.ru/interface-bus-iic-i2c.html; https://ru.wikipedia.org/wiki/I%C2%B2C */

#include <avr/io.h>
#include <avr/delay.h>
#include <compat/twi.h>

#define F_CPU 8000000UL

void TwiInit()
{
	// SCL frequency = F_CPU / ((16 + 2*TWBR)*PrescalerValue)
	TWSR = 0x00; // reset Status Register, and set no prescaling (TWPS0, TWPS1)
	TWBR = 0x06; // set Bit Rate to 100Khz
}

// Send START signal
void TwiStart()
{
	// The application writes the TWSTA bit to one when it desires to become a Master.
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN); // START condition (atmega8 datasheet page 170)
	
	// More info about TWINT - atmega8 datasheet page 165
	while (!(TWCR & (1<<TWINT))); // wait for TWINT Flag set. This indicates that the START condition has been transmitted.
}

// Send STOP signal
void TwiStop()
{
	TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN); // STOP condition (atmega8 datasheet page 170)
	while(TWCR & (1<<TWSTO)); // wait until stop condition is executed and bus released
}

void TwiWrite(uint8_t data)
{
	TWDR = data; // load data into Data Register. 
	TWCR = (1<<TWINT)|(1<<TWEN); // atmega8 datasheet page 170
	while (!(TWCR & (1<<TWINT))); // wait for TWINT Flag set. This indicates that the DATA has been transmitted
}

// Use it to read all bytes from slave except the last one
uint8_t TwiReadACK()
{
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA); // enable Acknowledge bit in TWCR
	while (!(TWCR & (1<<TWINT))); // wait while reading
	return TWDR;
}

// Use it to read the last byte from slave (until NACK is received, line is occupied)
uint8_t TwiReadNACK()
{
	TWCR = (1<<TWINT)|(1<<TWEN); // disable Acknowledge bit in TWCR
	while (!(TWCR & (1<<TWINT))); // wait while reading
	return TWDR;
}

// Use it for detecting failures
uint8_t TwiGetStatus()
{
	uint8_t status;
	status = TWSR & 0xF8; // upper five bits from TWSR (atmega8 datasheet page 170)
	return status;
}


int main(void)
{	
    TwiInit();
	
    while(1)
    {
        TwiStart();
        TwiWrite(0xf3); // just send some data here 
        TwiWrite(0x0f); 
        TwiStop();
		
        _delay_ms(1000);
    }
}