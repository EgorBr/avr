﻿#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/delay.h>
#include "Nokia5110.h"

int main(void)
{	
	LcdInit();
	LcdDrawLine(PIXEL_ON, 0,9, 83, 15);
	LcdDrawCircle(PIXEL_ON, 8, 20, 5);
	LcdDrawRect(PIXEL_ON, 18, 20, 5, 5);
	LcdDrawFillRect(PIXEL_ON, 28, 20, 5, 5);
	LcdDrawChar(PIXEL_ON, 35, 20, '9');
	LcdDrawString(PIXEL_ON, 0, 0, "Hello World!");
	
	while(1)
	{
		
	}
}